/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service.scraper;

import java.io.InputStream;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream;
import java.io.StringWriter;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;

/**
 *
 * @author tyanthoneym
 */
public class MassScraper {

    public static void main(String args[]) throws InterruptedException, Exception {
        /*
        // set up trust manager
        // needed to prevent unwanted exceptions
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) { }
                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) { }

                }
        };
        
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            System.out.println("Caught a trust exception.");
            e.printStackTrace();
        }
        
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) { return true; }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
         End of the fix*/
        
        TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
        SSLSocketFactory sf = new SSLSocketFactory(
          acceptingTrustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("https", 8443, sf));
        ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);

        // set the base url for the site that will be scraped
        String baseUrl = "https://www.findmassmoney.com/app/claim-status-search";
        
        // for recaptcha
        String googleDataSiteKey = "6LcJyBAUAAAAAJ0NqcrAZZ0HfF5ZL_cMseObwCpn";
        
        // set up TwoCaptcha
        TwoCaptchaService service = new TwoCaptchaService("aa378678a33777de7874dcb3b8a43c1b", googleDataSiteKey, baseUrl);
        String solution = "";
        try {
            solution = service.solveCaptcha();
        } catch (Exception e) {
            System.out.println("captcha failure");
            e.printStackTrace();
        }
        
        System.out.println("Captcha Token\n" + solution);
        
        String stateId = "3872921";
        
        // now issue the get request with the captcha token in the header
        DefaultHttpClient client = new DefaultHttpClient(ccm);
        String getURL = "https://www.findmassmoney.com/SWS/claims/" + stateId + "/statuses";
        HttpGet get = new HttpGet(getURL);
        get.setHeader("X-SWS-Recaptcha-Token", solution);
        
        System.out.println("About to execute get");
        HttpResponse responseGet = client.execute(get);
        HttpEntity entity = responseGet.getEntity();
        String output = "";
        if (entity != null) {
            InputStream inputStream = entity.getContent();
            try {
                // conver the input stream to a string
                StringWriter writer = new StringWriter();
                IOUtils.copy(inputStream, writer, "UTF-8");
                output = writer.toString();
            } finally {
                inputStream.close();
            }
        } else {
            System.out.println("Something went wrong returning null");
        }
        
        System.out.println("output\n" + output);
    }

}

