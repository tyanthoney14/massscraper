/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service.scraper;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author tyanthoneym
 */
public class TwoCaptchaService {
    
    private String apiKey;
    private String googleKey;
    private String pageUrl;

    private HttpWrapper httpWrapper;

    public TwoCaptchaService(String apiKey, String googleKey, String pageUrl) {
        this.apiKey = apiKey;
        this.googleKey = googleKey;
        this.pageUrl = pageUrl;
         httpWrapper = new HttpWrapper();
    }

    public String solveCaptcha() throws InterruptedException, IOException {
        System.out.println("Sending recaptcha challenge to 2captcha.com");

        String parameters = "key=" + apiKey
                + "&method=userrecaptcha"
                + "&googlekey=" + googleKey
                + "&pageurl=" + pageUrl;


        httpWrapper.get("http://2captcha.com/in.php?" + parameters);

        String captchaId = httpWrapper.getHtml().replaceAll("\\D", "");
        int timeCounter = 0;
        
        System.out.println("Waiting for captcha to be solved...");
        do {
            httpWrapper.get("http://2captcha.com/res.php?key=" + apiKey
                    + "&action=get"
                    + "&id=" + captchaId);

            Thread.sleep(1000);

            timeCounter++;
        } while(httpWrapper.getHtml().contains("NOT_READY"));

        System.out.println("It took "  + timeCounter + " seconds to solve the captcha");
        String gRecaptchaResponse = httpWrapper.getHtml().replaceAll("OK\\|", "").replaceAll("\\n", "");
        return gRecaptchaResponse;
    }


    class HttpWrapper {
        private boolean printHeaders = false;
        private String html;
        private int responseCode = 0;

        public HttpWrapper() {
            html = "";
        }
        public void get(String url) {

            try {
                URL url_ = new URL(url);
                HttpURLConnection conn;

                conn = (HttpURLConnection) url_.openConnection();
                conn.setRequestMethod("GET");
                conn.setAllowUserInteraction(false);
                conn.setDoOutput(false);
                conn.setInstanceFollowRedirects(false);

                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0");
                conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                conn.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
                conn.setRequestProperty("Connection", "keep-alive");

                String headers = "";

                if (printHeaders) {
                    for(String key: conn.getHeaderFields().keySet())
                        headers += ((key != null)?key + ": ":"") + conn.getHeaderField(key) + "\n";
                }

                responseCode = conn.getResponseCode();

                BufferedReader d = new BufferedReader(new InputStreamReader(new DataInputStream(conn.getInputStream())));
                String result = "";
                String line = null;
                while ((line = d.readLine()) != null) {
                    line = new String(line.getBytes(),"UTF-8");
                    result += line + "\n";
                }

                d.close();

                if (printHeaders) {
                    setHtml(headers + "\n" + result);
                } else {
                    setHtml(result);
                }
            } catch (IOException e) {
                throw new IllegalStateException("An IOException occurred:" + "\n" + e.getMessage());
            }
        }

        public String getHtml() {
            return this.html;
        }

        private void setHtml(String html) {
            this.html = html;
        }

        public void setPrintHeaders(boolean trueOrFalse) {
            printHeaders = trueOrFalse;
        }

        public int getResponseCode() {
            return responseCode;
        }

    }
}
